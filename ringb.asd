(defsystem "ringb"
  :version "0.1.0"
  :author "Michael Reis"
  :license "AGPL 3.0"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "ringb/tests"))))

(defsystem "ringb/tests"
  :author "Michael Reis"
  :license "AGPL 3.0"
  :depends-on ("ringb"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for ringb"
  :perform (test-op (op c) (symbol-call :rove :run c)))
