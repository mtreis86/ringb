(in-package :ringb)

#|
Second pass at a ring buffer. First things first I don't even understand my explanation of how
the original works. So I'll start there. We can easily test. I suspect the code itself is fine,
the description and or symbols are not. We can test tho to determine the behavior.
Examine the source and ignore the description.

We also can at this stage think about other ways I may need to run the ring buffer. For instance,
I am already mostly sure I do understand what it does and it just doesn't do what I want. So I may
need to add functionality rather than refactor. We'll see if I do any more than type this...
|#

