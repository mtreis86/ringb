(defpackage ringb
  (:use :cl)
  (:export
   :make-ringb
   :ringb-index
   :ringb-entry
   :next-index
   :next-entry
   :next-offset
   :ringb-offset
   :offset-entry
   :list-to-ringb))
(in-package :ringb)

#|
This is a ring buffer with a twist, consider it a rotating ring. Rather than just a ring buffer with
a fill pointer pointing into the ring, this has an additional pointer that sets the zeroth cell
according to the fill pointer. I refer to the fill pointer as the index, and the other pointer as
the offset. In other words, the offset sets the ring's rotation, while the index sets the usual
fill position. Like a normal ring buffer the contents of the ring aren't shifted around when the
index or offset move, rather a calculation is made on every access to determine the real location
of the data given the offset and index. If the offset is zero the ring acts normally. The ringb
is not currently adjustable, there is no resizing functionality.
|#

(defstruct (ringb (:constructor %make-ringb%))
  index offset size vector)

(defun make-ringb (size)
  (%make-ringb%
   :index 0
   :offset 0
   :size size
   :vector (make-array size :adjustable nil :fill-pointer nil)))

(defun (setf ringb-entry) (entry ringb &optional index)
  "Set an entry without offset. If no index provided, use the ringb's."
  (setf (aref (ringb-vector ringb) (if index index (ringb-index ringb))) entry))

(defun ringb-entry (ringb &optional index)
  "Get the entry at the index provided or in the ringb if not."
  (aref (ringb-vector ringb) (if index index (ringb-index ringb))))

(defun next-index (ringb)
  "Advance the index one step. Wraps."
  (setf (ringb-index ringb) (mod (1+ (ringb-index ringb))
                                 (ringb-size ringb))))

(defun offset-entry (ringb &optional cell-index)
  "Get the entry at the index accounting for the offset."
  (ringb-entry ringb (mod (+ (if cell-index cell-index (ringb-index ringb))
                             (ringb-offset ringb))
                          (ringb-size ringb))))

(defun (setf offset-entry) (entry ringb &optional cell-index)
  "Set the entry at the index accounting for the offset"
  (setf (ringb-entry ringb (mod (+ (if cell-index cell-index (ringb-index ringb)) (ringb-offset ringb))
                                (ringb-size ringb)))
        entry))

(defun next-offset (ringb)
  "Advance the offset one step."
  (setf (ringb-offset ringb) (mod (1+ (ringb-offset ringb))
                                  (ringb-size ringb))))

(defun list-to-ringb (list ringb)
  "Step through the list and the ring buffer copying the contents of the list to the cells of the
  buffer. If either runs out, leaves the index pointing to the next open space if any."
  (loop for entry in list
        for index from (ringb-index ringb) below (+ (ringb-index ringb) (ringb-size ringb))
        for offset-index = (mod index (ringb-size ringb))
        do (setf (offset-entry ringb offset-index) entry)))

(defun print-ringb (ringb &optional (stream t))
  (format stream "~&RING BUFFER:~%INDEX: ~A OFFSET: ~A SIZE: ~A~%ENTRIES: "
          (ringb-index ringb) (ringb-offset ringb) (ringb-size ringb))
  (print-vector (ringb-vector ringb) stream))

(defmethod print-object ((ringb ringb) (stream t))
  (print-vector (ringb-vector ringb) stream))

(defun print-vector (vector &optional (stream t))
  (loop for entry across vector
        do (format stream "~A " entry)
        finally (format stream "~&")))
