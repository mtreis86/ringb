(defpackage ringb/tests/main
  (:use :cl
        :ringb
        :rove))
(in-package :ringb/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :ringb)' in your Lisp.

(deftest ringbuffers
  (testing "make-ringb produces ring buffer types"
    (ok (typep (make-ringb 6) 'ringb::ringb)))
  (testing "ringb has an index int initialized to zero"
    (ok (= 0 (ringb::ringb-index (make-ringb 6)))))
  (testing "ringb has a size int"
    (ok (= 6 (ringb::ringb-size (make-ringb 6)))))
  (testing "ringb has a vector of size "
    (ok (= 6 (array-dimension (ringb::ringb-vector (make-ringb 6)) 0))))
  (testing "entry gets the content of the given index'd cell"
    (ok (= 5 (let ((ringb (make-ringb 6)))
               (setf (aref (ringb::ringb-vector ringb) 5) 5)
               (ringb-entry ringb 5)))))
  (testing "entry defaults to current index"
    (ok (= 0 (ringb-entry (make-ringb 6)))))
  (testing "setf entry sets at given index"
    (ok (= 3 (let ((ringb (make-ringb 6)))
               (setf (ringb-entry ringb 3) 3)
               (aref (ringb::ringb-vector ringb) 3)))))
  (testing "setf entry can't set to index at or above size"
    (signals (setf (ringb-entry (make-ringb 6)) 6)))
  (testing "setf entry sets at the current index if none given"
    (ok (= 2 (let ((ringb (make-ringb 6)))
               (setf (ringb-entry ringb) 2)
               (aref (ringb::ringb-vector ringb) 0)))))
  (testing "next-index increases the index if below size"
    (ok (= 5 (let ((ringb (make-ringb 6)))
               (setf (ringb-index ringb) 4)
               (next-index ringb)
               (ringb-index ringb)))))
  (testing "next-index rolls the index to zero at or above size"
    (ok (= 0 (let ((ringb (make-ringb 6)))
               (setf (ringb-index ringb) 5)
               (next-index ringb)
               (ringb-index ringb)))))
  (testing "setf index errors if trying to set at or above size"
    (signals (setf (ringb-index (make-ringb 6)) 6)))
  (testing "offset-entry gets the cell at the cell-index plus the index of the ringb"
    (ok (= 3 (let ((ringb (make-ringb 6)))
               (loop for index from 0 below 6
                     do (setf (ringb::ringb-entry ringb index)
                              index))
               (setf (ringb-offset ringb) 3)
               (offset-entry ringb 0)))))
  (testing "offset-entry wraps around when the offset + cell-index is above the size"
    (ok (= 1 (let ((ringb (make-ringb 6)))
               (loop for index from 0 below 6
                     do (setf (ringb::ringb-entry ringb index)
                              index))
               (setf (ringb-offset ringb) 3)
               (offset-entry ringb 4)))))
  (testing "stepping the offset incfs the offset"
    (ok (= 4 (let ((ringb (make-ringb 6)))
               (loop for index from 0 below 6
                     do (setf (ringb::ringb-entry ringb index)
                              index))
               (setf (ringb-offset ringb) 3)
               (step-ringb ringb)
               (ringb-offset ringb)))))
  (testing "stepping the offset past or at the size rolls it back around to zero"
    (ok (= 0 (let ((ringb (make-ringb 6)))
               (loop for index from 0 below 6
                     do (setf (ringb::ringb-entry ringb index)
                              index))
               (setf (ringb-offset ringb) 5)
               (step-ringb ringb)
               (ringb-offset ringb))))))

